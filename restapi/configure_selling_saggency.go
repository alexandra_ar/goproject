// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	"crypto/tls"
	"database/sql"
	"fmt"
	"golangwork/models"
	"log"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"golangwork/restapi/operations"
	"golangwork/restapi/operations/product"
	"golangwork/restapi/operations/user"

	_ "github.com/go-sql-driver/mysql"
)

//go:generate swagger generate server --target ../../golangwork --name SellingSaggency --spec ../swagger.yml

func configureFlags(api *operations.SellingSaggencyAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

type userFormat struct {
	username string `json:"username"`
	email    string `json:"email"`
}

func configureAPI(api *operations.SellingSaggencyAPI) http.Handler {
	// configure the api here
	api.ServeError = errors.ServeError

	// Set your custom logger if needed. Default one is log.Printf
	// Expected interface func(string, ...interface{})
	//
	// Example:
	// api.Logger = log.Printf

	api.JSONConsumer = runtime.JSONConsumer()

	api.JSONProducer = runtime.JSONProducer()

	db, err := sql.Open("mysql", "root:1234@tcp(localhost:3306)/golang?charset=utf8")
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err)
	}

	var userList []*models.User
	var productList []*models.Product

	rows, err := db.Query(fmt.Sprintf("SELECT NAME, COUNT FROM products"))
	for rows.Next() {
		post := &models.Product{}
		err = rows.Scan(&post.Name, &post.Count)
		if err != nil {
			panic(err)
		}
		productList = append(productList, post)
	}
	rows.Close()

	rows, err = db.Query(fmt.Sprintf("SELECT USERNAME, EMAIL, ROLE FROM users"))
	for rows.Next() {
		post := &models.User{}
		err = rows.Scan(&post.Username, &post.Email, &post.Role)
		if err != nil {
			panic(err)
		}
		userList = append(userList, post)
	}
	fmt.Println(userList)
	// надо закрывать соединение, иначе будет течь
	rows.Close()

	log.Printf("WAS ok with db")

	api.UserGetHandler = user.GetHandlerFunc(func(params user.GetParams) middleware.Responder {

		//fmt.Println(jsonStr)
		return user.NewGetOK().WithPayload(userList)
	})

	api.ProductListHandler = product.ListHandlerFunc(func(params product.ListParams) middleware.Responder {
		log.Println(len(productList), &params.ByName)

		if params.ByName == nil && params.Gt == nil && params.Lt == nil && params.Eq == nil {
			log.Println(productList, &params.ByName, params.Gt, params.Lt, params.Eq)
			return product.NewListOK().WithPayload(productList)
		}
		var products []*models.Product

		for _, prod := range productList {
			log.Println(productList, "hhhh")
			if *params.ByName == *prod.Name {
				products = append(products, prod)
			}
		}
		log.Println(productList)
		return product.NewListOK().WithPayload(products)
	})

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}